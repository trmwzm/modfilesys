modFileSys: Fortran File System Interface
=========================================

The library modFileSys is designed to provide modern (Fortran 2003) wrappers
around the file system interface of libc. With the current implementation you
can conveniently

* rename and remove files,
* create and remove directories (latter also recursive),
* obtain the name of files in a given directory,
* check whether a given file exists and whether it is a directory or
  a symbolic link,
* obtain the current working directory and change to a directory,
* create hard links and symbolic links, resolve the target of symbolic links,
* canonize path names.

Allmost all routines take and return filenames (Fortran character variables),
providing an abstract interface without exposing internal details about the
operating system. The library should work on any POSIX compliant systems.

The library is available under the 2-clause BSD license.
