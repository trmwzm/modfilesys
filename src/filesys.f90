!> Wrapper routines around libc's file system interface.
module filesys_module
  use libc_interface_module
  use fsys_interface_module
  use filesys_common_module
  implicit none
  private

  public :: dirdesc
  public :: unlink
  public :: rmdir
  public :: remove
  public :: rename
  public :: opendir
  public :: isdir
  public :: islink
  public :: file_exists
  public :: mkdir
  public :: rmtree
  public :: getcwd
  public :: chdir
  public :: link
  public :: symlink
  public :: readlink
  public :: realpath

  
  !> Directory descriptor.
  type :: dirdesc
    private
    type(c_ptr) :: cptr = c_null_ptr
  contains
    !> Returns next entry in the directory.
    procedure :: next_filename => dirdesc_next_filename

    !> Destructs a directory descriptor.
    final :: dirdesc_destruct
  end type dirdesc


contains

  !> Unlinks a file.
  !! \param filename  Name of the file.
  !! \param error  Error value of the libc unlink function. If not present and
  !!     different from zero, the program stops.
  subroutine unlink(filename, error)
    character(*,kind=c_char), intent(in) :: filename
    integer(c_int), intent(out), optional :: error

    integer(c_int) :: error0

    error0 = libc_unlink(f_c_string(filename))
    call handle_errorcode(error0, "Call 'unlink' in 'unlink'", error)
    
  end subroutine unlink


  !> Removes an empty directory.
  !! \param filename  Name of the directory.
  !! \param error  Error value of the libc rmdir function. If not present and
  !!     different from zero, the program stops.
  subroutine rmdir(filename, error)
    character(*,kind=c_char), intent(in) :: filename
    integer(c_int), intent(out), optional :: error

    integer(c_int) :: error0

    error0 = libc_rmdir(f_c_string(filename))
    call handle_errorcode(error0, "Call 'rmdir' in 'rmdir'", error)

  end subroutine rmdir


  !> Removes a file or an empty directory.
  !! \param filename  Name of the file.
  !! \param error  Error value of the libc unlink function. If not present and
  !!     different from zero, the program stops.
  subroutine remove(filename, error)
    character(*,kind=c_char), intent(in) :: filename
    integer(c_int), intent(out), optional :: error

    integer(c_int) :: error0

    error0 = libc_remove(f_c_string(filename))
    call handle_errorcode(error0, "Call 'remove' in 'remove'", error)

  end subroutine remove

  
  !> Renames a file.
  !! \param oldname  Old file name.
  !! \param newname  New file name.
  !! \param error  Error value of the libc rename function. If not present and
  !!     different from zero, the program stops.
  subroutine rename(oldname, newname, error)
    character(*,kind=c_char), intent(in) :: oldname, newname
    integer(c_int), intent(out), optional :: error

    integer(c_int) :: error0

    error0 = libc_rename(f_c_string(oldname), f_c_string(newname))
    call handle_errorcode(error0, "Call 'rename' in 'rename'", error)

  end subroutine rename


  !> Creates a directory with the default permissions.
  !! \param dirname  Name of the directory to create.
  !! \param error  Error value of the libc mkdir function. If not present and
  !!     different from zero, the program stops.
  subroutine mkdir(dirname, error)
    character(*, kind=c_char), intent(in) :: dirname
    integer(c_int), intent(out), optional :: error

    integer(c_int) :: error0

    error0 = fsys_makedir(f_c_string(dirname))
    call handle_errorcode(error0, "Call 'makedir' in mkdir",&
        & error)

  end subroutine mkdir


  !> Removes recursively a file.
  !! \param fname  File name.
  !! \param error  0 if deletition was successfull, some negative number
  !!     otherwise. If not present and different from zero, the program stops.
  subroutine rmtree(fname, error)
    character(*, kind=c_char), intent(in) :: fname
    integer(c_int), intent(out), optional :: error

    integer(c_int) :: error0

    error0 = fsys_rmtree(f_c_string(fname))
    call handle_errorcode(error0, "Call 'rmtree' in rmtree", error)

  end subroutine rmtree


  !> Returns a descriptor to a directory.
  !! \param dirname  Name of the directory.
  !! \param dirptr  Directory descriptor on return.
  !! \param error  Error value of the libc opendir function. If not present and
  !!     different from zero, the program stops.
  subroutine opendir(dirname, dirptr, error)
    character(*, kind=c_char), intent(in) :: dirname
    type(dirdesc), intent(out) :: dirptr
    integer(c_int), intent(out), optional :: error

    integer(c_int) :: error0

    dirptr%cptr = libc_opendir(f_c_string(dirname))
    if (c_associated(dirptr%cptr)) then
      error0 = 0
    else
      error0 = -1
    end if
    call handle_errorcode(error0, "Call 'opendir' in 'opendir'", error)

  end subroutine opendir


  !> Checks whether a file with given name exists.
  !! \param fname  File name.
  !! \return True if file exists, False otherwise.
  function file_exists(fname) result(res)
    character(*, kind=c_char), intent(in) :: fname
    logical :: res

    res = (fsys_file_exists(f_c_string(fname)) /= 0)

  end function file_exists

  
  !> Checks whether a file with a given name exists and is a directory.
  !! \param fname  File name.
  !! \return True if file exists and is a directory, False otherwise.
  function isdir(fname) result(res)
    character(*, kind=c_char), intent(in) :: fname
    logical :: res

    integer(c_int) :: status

    status = fsys_isdir(f_c_string(fname))
    res = (status /= 0)
    
  end function isdir


  !> Checks whether a file with a given name exists and is a symbolic link.
  !! \param fname  File name.
  !! \return True if file exists and is a symlink, False otherwise.
  function islink(fname) result(res)
    character(*, kind=c_char), intent(in) :: fname
    logical :: res

    integer(c_int) :: status

    status = fsys_islink(f_c_string(fname))
    res = (status /= 0)
    
  end function islink


  !> Returns the name of the current working directory.
  !! \return Name of the current working directory or the empty string if
  !!     any error occured.
  function getcwd() result(res)
    character(:, kind=c_char), allocatable :: res

    type(c_ptr) :: pstr

    pstr = fsys_getcwd()
    if (c_associated(pstr)) then
      call cptr_f_string(pstr, res, dealloc=.true.)
    else
      res = ""
    end if

  end function getcwd


  !> Changes the directory.
  !! \param fname  Name of the new directory.
  !! \param error  Error value of the libc chdir function. If not present and
  !!     different from zero, the program stops.
  subroutine chdir(fname, error)
    character(*, kind=c_char), intent(in) :: fname
    integer(c_int), intent(out), optional :: error

    integer(c_int) :: error0

    error0 = libc_chdir(f_c_string(fname))
    call handle_errorcode(error0, "Call 'chdir' in 'chdir'", error)

  end subroutine chdir


  !> Creates a hard link.
  !! \param oldname  Name of the existing file.
  !! \param newname  Name of the new link.
  !! \param error  Error value of the libc link function. If not present and
  !!     different from zero, the program stops.
  subroutine link(oldname, newname, error)
    character(*, kind=c_char), intent(in) :: oldname, newname
    integer(c_int), intent(out), optional :: error

    integer(c_int) :: error0

    error0 = libc_link(f_c_string(oldname), f_c_string(newname))
    call handle_errorcode(error0, "Call 'link' in 'link'", error)

  end subroutine link


  !> Creates a symbolic link.
  !! \param oldname  Name of the existing file.
  !! \param newname  Name of the new link.
  !! \param error  Error value of the libc symlink function. If not present and
  !!     different from zero, the program stops.
  subroutine symlink(oldname, newname, error)
    character(*, kind=c_char), intent(in) :: oldname, newname
    integer(c_int), intent(out), optional :: error

    integer(c_int) :: error0

    error0 = libc_symlink(f_c_string(oldname), f_c_string(newname))
    call handle_errorcode(error0, "Call 'link' in 'link'", error)

  end subroutine symlink


  !> Resolves a link.
  !! \param fname  Name of the file to resolve.
  !! \return  Resolved link name or empty string if any error occured.
  function readlink(fname) result(res)
    character(*, kind=c_char), intent(in) :: fname
    character(:, kind=c_char), allocatable :: res

    type(c_ptr) :: ptr

    ptr = fsys_readlink(f_c_string(fname))
    if (c_associated(ptr)) then
      call cptr_f_string(ptr, res, dealloc=.true.)
    else
      res = ""
    end if
    
  end function readlink


  !> Returns the real (canonized) name of a path.
  !! \param fname  Path to resolve.
  !! \return Real path name or empty string if any error occured.
  function realpath(fname) result(res)
    character(*, kind=c_char), intent(in) ::fname
    character(:, kind=c_char), allocatable  :: res

    type(c_ptr) :: cname, cresolved, rpath

    rpath = c_null_ptr
    cname = f_cptr_string(fname)
    if (c_associated(cname)) then
      cresolved = c_null_ptr
      rpath = libc_realpath(cname, cresolved)
    end if
    if (c_associated(rpath)) then
      call cptr_f_string(rpath, res, dealloc=.true.)
    else
      res = ""
    end if

  end function realpath
    

  !> Returns the name of the next entry in a directory (without "." and "..").
  !! \param self  Directory descriptor.
  !! \return  Name of the next entry or empty string if error occured.
  function dirdesc_next_filename(self) result(fname)
    class(dirdesc), intent(inout) :: self
    character(:, kind=c_char), allocatable :: fname

    type(c_ptr) :: cptr

    cptr = fsys_nextdirentry_name(self%cptr)
    if (c_associated(cptr)) then
      call cptr_f_string(cptr, fname, dealloc=.true.)
    else
      fname = ""
    end if

  end function dirdesc_next_filename

  
  !> Destructs a directory descriptor.
  !! \param self  Directory descriptor instance.
  subroutine dirdesc_destruct(self)
    type(dirdesc), intent(inout) :: self

    call libc_closedir(self%cptr)

  end subroutine dirdesc_destruct


end module filesys_module
