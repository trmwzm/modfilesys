!> Wrappers for the C-functions in the libcfx library.
module fsys_interface_module
  use, intrinsic :: iso_c_binding
  implicit none
  
  interface
    !> Delivers the next entry within a directory.
    function fsys_nextdirentry_name(dirptr) bind(c) result(res)
      import :: c_ptr
      type(c_ptr), value :: dirptr
      type(c_ptr) :: res
    end function fsys_nextdirentry_name

    !> Returns the name field of a directory entry.
    function fsys_direntry_name(direntry) bind(c) result(res)
      import :: c_ptr
      type(c_ptr), value :: direntry
      type(c_ptr) :: res
    end function fsys_direntry_name

    !> Decides whether a given file name is a directory.
    function fsys_isdir(fname) bind(c) result(res)
      import :: c_int, c_char
      character(kind=c_char), intent(in) :: fname(*)
      integer(c_int) :: res
    end function fsys_isdir

    !> Decides whether a given file name is a symbolic link.
    function fsys_islink(fname) bind(c) result(res)
      import :: c_int, c_char
      character(kind=c_char), intent(in) :: fname(*)
      integer(c_int) :: res
    end function fsys_islink

    !> Checks, whether a given file exists.
    function fsys_file_exists(fname) bind(c) result(res)
      import :: c_int, c_char
      character(kind=c_char), intent(in) :: fname(*)
      integer(c_int) :: res
    end function fsys_file_exists

    !> Creates a directory with all possible permitions (except those in umask).
    function fsys_makedir(dirname) bind(c) result(res)
      import :: c_int, c_char
      character(kind=c_char), intent(in) :: dirname(*)
      integer(c_int) :: res
    end function fsys_makedir

    !> Recursively deletes an entry in the file system.
    function fsys_rmtree(dirname) bind(c) result(res)
      import :: c_int, c_char
      character(kind=c_char), intent(in) :: dirname(*)
      integer(c_int) :: res
    end function fsys_rmtree

    !> Returning working directory name with dynamic allocation as in glibc.
    function fsys_getcwd() bind(c) result(res)
      import :: c_ptr
      type(c_ptr) :: res
    end function fsys_getcwd

    !> Frees a character string.
    subroutine fsys_freestring(ptr) bind(c)
      import :: c_ptr
      type(c_ptr), value :: ptr
    end subroutine fsys_freestring

    !> Allocates a character string
    function fsys_copystring(origstr) bind(c) result(res)
      import :: c_char, c_ptr
      character(kind=c_char), intent(in) :: origstr(*)
      type(c_ptr) :: res
    end function fsys_copystring

    !> Calls the libc readlink function with dynamic memory allocation.
    function fsys_readlink(fname) bind(c) result(res)
      import :: c_char, c_ptr
      character(kind=c_char), intent(in) :: fname(*)
      type(c_ptr) :: res
    end function fsys_readlink
      
  end interface

end module fsys_interface_module
