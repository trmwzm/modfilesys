program test_ls
  use, intrinsic :: iso_c_binding
  use filesys_module

  type(dirdesc) :: dir
  character(:, kind=c_char), allocatable :: path
  
  write(*, "(A)") "Current directory:"
  path = getcwd()
  write(*, "(A)") path
  write(*, "(A)") "Cannonized name of current directory:"
  write(*, "(A)") realpath(path)
  write(*, "(A)") "Changing to directory '/tmp'"
  call chdir("/tmp")
  write(*, "(A)") "Listing current directory:"
  call opendir("./", dir)
  path = dir%next_filename()
  do while (len(path) > 0)
    write(*, "(A)") path
    path = dir%next_filename()
  end do
  
end program test_ls
