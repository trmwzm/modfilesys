program test_mkdir_rmtree
  use filesys_module

  character(128) :: filename
  logical :: exists


  filename = "_testdir"
  exists = file_exists(filename)
  write(*, "(A,L2)") "Testing whether '_testdir' exists: ", exists
  if (exists) then
    write(*, "(A,L2)") "Is it a directory? ", isdir(filename)
    write(*, "(A,L2)") "Is it a symbolic link?", islink(filename)
    write(*, "(A)") "Hit [enter] to delete it recursively or Ctrl-C to stop"
    read(*, *)
    call rmtree(filename)
  else
    write(*, "(A)") "Creating directory '_testdir'"
    call mkdir("_testdir")
    write(*, "(A)") "Creating directory '_testdir/test2'"
    call mkdir("_testdir/test2")
    write(*, "(A)") "Creating file '_testdir/test2/file'"
    open(12, file="_testdir/test2/file")
    close(12)
    write(*, "(A)") "Creating a symlink to it as '_testdir/symlink'"
    call symlink("_testdir/test2/file", "_testdir/symlink")
    write(*, "(A)") "Creating a hard ink to it as '_testdir/hardlink'"
    call link("_testdir/test2/file", "_testdir/hardlink")
    write(*, "(A)") "Directories created, invoke the program again to&
        & delete them"
  end if
  write(*, "(A)") "Done."

end program test_mkdir_rmtree
