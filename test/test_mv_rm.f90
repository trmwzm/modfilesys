program test_unlink
  use filesys_module

  write(*, "(A)") "Creating 'test1.dat'"
  open(12, file="test1.dat", form="formatted")
  write(12, *) "Hello 1"
  close(12)
  write(*, "(A)") "Renaming 'test1.dat' to 'test2.dat'"
  call rename("test1.dat", "test2.dat")
  write(*, "(A)") "Removing 'test2.dat'"
  call remove("test2.dat")
  write(*, "(A)") "Creating 'test3.dat'"
  open(12, file="test3.dat", form="formatted")
  write(12, *) "Hello 3"
  close(12)
  write(*, "(A)") "Unlinking 'test3.dat'"
  call unlink("test3.dat")

end program test_unlink
